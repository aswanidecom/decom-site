<?php

	require_once('config.php');
	require_once("$DELIBDIR/php/views/page.php");
	require_once($DELIBDIR.'/php/menu.php');
	require_once("$DELIBDIR/php/navigator.php");
	require_once($DELIBDIR.'/php/inst.php');
	require_once($DELIBDIR.'/php/sites.php');
	require_once($DELIBDIR.'/php/site.php');
	require_once($DELIBDIR.'/php/views/entity.php');
	require_once($DELIBDIR.'/php/view.php');
	require_once($DELIBDIR.'/php/people/person.php');
	require_once($DELIBDIR.'/php/query.php');
	
	decom_page_init();
        decom_page_set_header_logo_url('/pics/common/uoc-header-transp.png');


	$cont = '';
	$footer = new DecomPageViewFooter();

	if(!isset($_GET['inst'])) {
		$diRoot = new DecomInstitute(2);
		$schools = $diRoot->getChildrenIds();

		$menu = new DecomMenu();

		foreach($schools as $school) {
			$submenu = new DecomMenu();
			$diSchool = new DecomInstitute($school);
			$nav = new DecomNavigator();
			$depts = $diSchool->getChildrenIds();

			foreach($depts as $dept) {
				$diDept = new DecomInstitute($dept);
				$nav->setParameter('inst',$dept);
				$submenu->addItem(
					new DecomMenuItem(
						$diDept->getPropertyValue('name'),   // text
						$nav->toUrl(),                // url
						$diDept->getPropertyValue('name'))); // hint
			}

			$menu->addItem(
				new DecomMenuItem(
					$diSchool->getPropertyValue('name'),
					'#',
					$diSchool->getPropertyValue('name'),$submenu));
		}

		$cont .= $menu->toHtml('_deBlockList _deBlockListMinH10em _deBlockListW95 _deCols3');
	}	
	else { // $_GET['inst'] is set
		$did = $_GET['inst'];     //TODO error check        
		
		$iobj = new DecomInstitute($did);
		$icode = $iobj->getPropertyValue('code');
		
		$v = new DecomView();
		
		$title = $iobj->getPropertyValue('name');
		decom_page_set_title($title);

		$site = $iobj->getRelativeFirst('has', 'site'); // TODO errcheck
		if(decom_is_errobj($site)) {
			echo 'Error 89899'; // TODO FIXME
			exit;
		}
		
		$sobj = new DecomSite($site);

		$navbar = $sobj->getNavigationMenu($_GET,'page');
		if(decom_is_errobj($navbar))
			decom_page_add_error_message($navbar->getMessageHtml());
		else if($navbar !== null)
			decom_page_set_navbar($navbar);
		
        	//$sidemenu = new DecomMenu();
		//$sidemenu->addItem(new DecomMenuItem('Events', '/', 'Events'));
        	//decom_page_set_side_menu($sidemenu);
        	
        	//$fmenu = new DecomMenu();
		//$fmenu->addItem(new DecomMenuItem('Home', '#', 'Home page'));
		// TODO $fmenu->addItem(new DecomMenuItem('Contact Us', '$iobj->getPropertyValue('phone')', 'Second page'));
		
		$footer = new DecomPageViewFooter();
		$footer->setCustomHtml($iobj->getPropertyValueFirst('phone'). '<p>Copyright (C) 2019 Calicut university.</p>');
		
		// TODO FIXME
		//$footer->setMenu($fmenu);
		//$footer->setAttributionHtml('<p>Copyright (C) 2019 Calicut university.</p>');
	
        	$cont = '';
        	$cont .= '<img style="max-width: 800px" src="/pics/'.$icode.'/banner.jpg"/><br/>';
         	
         	if(isset($_GET['page'])) {
			$path = &$_GET['page'];

			$pid = $sobj->getPageIdFromPath($path);		//TODO future use

			if(decom_is_errobj($pid)) {
				echo $pid->getMessageHtml();
				echo 'Error jksdhfsdf'; exit; // TODO FIXME
			}
			else if($pid === null) {
	
		//		$pobj = new DecomSitePage($pid);
		//		$pcode = $pobj->getPropertyValue('code');
		
				switch($_GET['page']) {
					case 'home'	:$cont.='<h2>Welcome to '.$title.'</h2><br/>';
							$cont .= '<p>'.$iobj->getPropertyValue('description').'</p><br/>';
							$cont.='<hr/>';
							$nfids= $iobj->getInverseRelative('to');
							$cont.='<h3>Notifications</h3><br/>';
							foreach($nfids as $nfid) {
								$noti = new DecomEntity('notification',$nfid);
								if($noti->getPropertyValue('dead')==0){
								$cont.='<ul><li>'.$noti->getPropertyValue('plaintxt_description').'</li></ul>';
								}
							}
							$cont.='<hr/>';
							break;
					case 'about/profile' :$cont .= '<h3>Profile</h3>';
							 	$cont .= $iobj->getPropertyValue('description');
								break; 
					case 'contact'	:$cont .= '<h3>Contact Us</h3>';
							 echo $iobj->getPropertyValueFirst('phone');
							 //$cont .= $iobj->getPropertyValueSecond('phone');
							break;
					case 'people/hod'	:
							
							
							$headid = $iobj->getInverseRelativeFirst('head_of');
							$pobj = new DecomEntity('person',$headid);
							$cont .='<h3>Head of '.$title.'</h3>';
							$cont.='';
							$cont.='<div  >Name     :'.$pobj->getPropertyValue('first_name').' '.$pobj->getPropertyValue('middle_name').' '.$pobj->getPropertyValue('last_name').'<br/>'.'Status  :'.$pobj->getPropertyValue('status').'</div>';
							$cont.='<div id="more"></div><button id="btnmore">Read more</button>';
							
							
							break;
				
					case 'people/faculty'	:
							$cont .='<h3>Faculties of '.$title.'</h3>';
							$headid = $iobj->getInverseRelativeFirst('head_of');
							$hobj = new DecomEntity('person',$headid);
							$cont.='Name   :'.$hobj->getPropertyValue('first_name').' '.$hobj->getPropertyValue('middle_name').' '.$hobj->getPropertyValue('last_name').'<br/>';
							//$cont .='Email id:'.$hobj->getPropertyValueFirst('email_official').'<br/>';
							$cont .='Status:'.$hobj->getPropertyValue('status');
							$cont.='<div id="more"></div><button id="btnmore">Read more</button>';
							$cont.='<hr/>';
							$facids= $iobj->getInverseRelative('faculty_of');
							foreach($facids as $facid) {
								$pobj = new DecomEntity('person',$facid);
								if($pobj->getPropertyValue('inactive')==0){
								$cont .='Name	:'.$pobj->getPropertyValue('first_name').' '.$pobj->getPropertyValue('middle_name').' '.$pobj->getPropertyValue('last_name').'<br/>';
								$cont .='Status  :'.$pobj->getPropertyValue('status').'<br/>';
								$cont.='<div id="more"></div><button id="btnmore">Read more</button>';
								$cont .='<hr/>';
								/*$v->load('entity', 'person',$facid);
								$cont .= $v->render();*/
								}}
							break;

					case 'people/staff'	:
							$staffids= $iobj->getInverseRelative('staff_of');	
							foreach($staffids as $staffid) {
								$pobj = new DecomEntity('person',$staffid);
								if($pobj->getPropertyValue('inactive')==0){
								$cont .='<h3>Staff </h3>';
								$cont.=$pobj->getPropertyValue('first_name').' '.$pobj->getPropertyValue('middle_name').' '.$pobj->getPropertyValue('last_name').'<br/>';
								$cont.='Status  :'.$pobj->getPropertyValue('status').'<br/>';
								$cont.='<div id="more"></div><button id="btnmore">Read more</button>';
								$cont.='<hr/>';
								}}
							break;
					default		:$cont .= $iobj->getPropertyValue('description');
							break; 
				}
			}
			else {
				// TODO display the contents of this concrete page
			}
		}else{$cont.='<h2>Welcome to '.$title.'</h2><br/>';
		$cont .= '<p>'.$iobj->getPropertyValue('description').'</p><br/>';
		$cont.='<hr/>';
		$nfids= $iobj->getInverseRelative('to');
		$cont.='<h3>Notifications</h3><br/>';
		foreach($nfids as $nfid) {
			$noti = new DecomEntity('notification',$nfid);
			if($noti->getPropertyValue('dead')==0){
			$cont.='<ul><li>'.$noti->getPropertyValue('plaintxt_description').'</li></ul>';
			}
		}$cont.='<hr/>';
							
							
							//$cont.='<h2>Our Vision is to:</h2>'.$iobj->getPropertyValue('vision_txt').'</p>';
							}
	}

	decom_page_set_footer($footer);
	decom_page_set_content($cont);
	decom_page_display();


?>
<script type="text/javascript">


document.addEventListener('DOMContentLoaded', function() {
	var val = " <?php echo $headid ?> ";
	btnmore = document.getElementById('btnmore');
	more = document.getElementById('more');
	btnmore.addEventListener('click', function() {
		xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200){
				more.innerHTML = this.response;
			}
		};
		xhr.open('GET', 'info.php?eid=2', true);
		xhr.send();
	});
});</script>
